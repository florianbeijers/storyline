Storyline::Application.routes.draw do
  #root
  root 'app#index'

  #game
  get '/game/nieuw'                                         => 'games#new', as: :new_game
  post '/game/create'                                        => 'games#create', as: :create_game
  get '/game/:id/verhaal-tot-nu-toe'                        => 'games#story', as: :game_story

  get '/game/:id'                                           => 'games#index', as: :game
  get '/game/:id/stap/een/'                                 => 'games#stepone', as: :game_step_one
  get '/game/:id/stap/twee/:writer'                         => 'games#steptwo', as: :game_step_two_writer
  get '/game/:id/stap/twee/'                                => 'games#steptwo', as: :game_step_two
  get '/game/:id/stap/drie/'                                => 'games#stepthree', as: :game_step_three

  #round
  patch '/game/:id/een/addtext'                             => 'rounds#add_text',   as: :game_step_one_add_text

  #submittedpicture
  get '/pictures/:string'                                   => 'submittedpictures#select'
  get '/picture/show/:id'                                  => 'submittedpictures#show', as: :show_picture
  get '/pictures/:string/:writer'                           => 'submittedpictures#select', as: :find_picture

  get '/game/:id/twee/addphoto/:flickr_id/:user_id/:writer' => 'submittedpictures#select_picture',  as: :game_step_two_add_photo
  get '/game/drie/stemmen/:id'                              => 'submittedpictures#vote_picture',    as: :game_step_two_three_vote_photo

  #user
  get '/gebruiker/zoeken/'                                  => 'users#index', as: :search_user_hard
  get '/gebruiker/zoeken/:string'                           => 'users#search', as: :search_user
  get '/gebruiker/berichten/'                               => 'users#messages', as: :user_messages
  get '/gebruiker/berichten/:id'                            => 'users#read_message', as: :read_message
  get '/gebruiker/berichten/:id/:status'                    => 'users#status_message', as: :status_message
  get '/aanmelden'                                          => 'users#new', as: :sign_up
  get '/vriend/toevoegen/:id'                               => 'users#addfriend', as: :add_friend
  resources :users

  #session
  get '/inloggen'                                           => 'sessions#new', as: :sign_in
  get '/uitloggen'                                          => 'sessions#destroy', as: :sign_out
  resources :sessions, only: [:new, :create, :destroy]
end
