class CreateSubmittedpictures < ActiveRecord::Migration
  def change
    create_table :submittedpictures do |t|
      t.integer :nrVotes
      t.boolean :isStartPicture
      t.boolean :isFinalPicture
      t.string :flickr_id
      t.references :round, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
