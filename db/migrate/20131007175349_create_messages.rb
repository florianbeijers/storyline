class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :title
      t.string :text
      t.references :user, index: true
      t.references :game, index: true
      t.references :friend, index: true
      t.references :round, index: true
      t.boolean :readed

      t.timestamps
    end
  end
end
