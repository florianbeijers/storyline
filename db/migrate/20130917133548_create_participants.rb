class CreateParticipants < ActiveRecord::Migration
  def change
    create_table :participants do |t|
      t.integer :score
      t.references :user, index: true
      t.references :game, index: true

      t.timestamps
    end
  end
end
