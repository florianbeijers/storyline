# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

def random_user
  User.offset(rand(User.count)).first
end

User.create(name: 'Willem van Lent', email: 'willemvanlent@live.nl', password: 'test', password_confirmation: 'test')

50.times do
  pass = Faker::Lorem.characters(10)
  User.create(name: Faker::Name.name, email: Faker::Internet.email, password: pass, password_confirmation: pass)
end

51.times do |i|
  user = User.find(i+1)
  5.times do
    user.friendships.create(friend_id: random_user.id)
  end
end

20.times do
  game = Game.create
  20.times do
    game.participants.create(user_id: random_user.id)
  end
end
