# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131107220023) do

  create_table "friendships", force: true do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "friendships", ["friend_id"], name: "index_friendships_on_friend_id"
  add_index "friendships", ["user_id"], name: "index_friendships_on_user_id"

  create_table "games", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: true do |t|
    t.string   "title"
    t.string   "text"
    t.integer  "user_id"
    t.integer  "game_id"
    t.integer  "friend_id"
    t.integer  "round_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "read"
  end

  add_index "messages", ["friend_id"], name: "index_messages_on_friend_id"
  add_index "messages", ["game_id"], name: "index_messages_on_game_id"
  add_index "messages", ["round_id"], name: "index_messages_on_round_id"
  add_index "messages", ["user_id"], name: "index_messages_on_user_id"

  create_table "participants", force: true do |t|
    t.integer  "score"
    t.integer  "user_id"
    t.integer  "game_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "participants", ["game_id"], name: "index_participants_on_game_id"
  add_index "participants", ["user_id"], name: "index_participants_on_user_id"

  create_table "rounds", force: true do |t|
    t.string   "storyFragment"
    t.integer  "game_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rounds", ["game_id"], name: "index_rounds_on_game_id"

  create_table "submittedpictures", force: true do |t|
    t.integer  "nrVotes"
    t.boolean  "isStartPicture"
    t.boolean  "isFinalPicture"
    t.string   "flickr_id"
    t.integer  "round_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "submittedpictures", ["round_id"], name: "index_submittedpictures_on_round_id"
  add_index "submittedpictures", ["user_id"], name: "index_submittedpictures_on_user_id"

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
