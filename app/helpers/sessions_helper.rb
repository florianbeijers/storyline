module SessionsHelper
  def current_user
    @_current_user ||= session[:current_user_id] && User.find_by_id(session[:current_user_id])
  end

  def signed_in?
    !current_user.nil? # the usual checks, nothing to see here. move along
  end
end
