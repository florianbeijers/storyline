class SessionsController < AppController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      sign_in user
      flash[:succes] = 'Het aanmelden is gelukt ...als het goed is'
      redirect_to user
    else
      flash[:error] = 'Iets ...ik weet niet wat maar ...iets ... klopt er niet '
      render 'new'
    end
  end

  def destroy
    sign_out
    redirect_to root_path
  end
end
