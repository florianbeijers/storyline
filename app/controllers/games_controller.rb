class GamesController < AppController
  def index
    @game = Game.find(params[:id])

    if @game.rounds.count == 0
      @round = Round.create(game: @game)
    end
  end

  def new
  end

  def create

    if(params.has_key?(:users) and !params[:users].nil?)
      game = Game.create()
      params[:users].each do |user_id|
        Message.create(:user_id => user_id, :title => params[:title], :text => params[:text], :game_id => game.id, :read => false)
      end
      flash[:succes] = "Uitnodigingen succesvol aangemaakt"
      redirect_to game
    else
      flash[:error] = "Selecteer ten minste een gebruiker om uitnodiging te versturen"
      render 'new'
    end

  end

  def story
    @game = Game.find(params[:id])
  end

  def stepone
    must_be_signed_in
    @game = Game.find(params[:id])

    @round = @game.rounds.last
  end

  def steptwo
    must_be_signed_in
    @game = Game.find(params[:id])

    if params[:writer].nil?
      @writer = 0
    else
      @writer = params[:writer]
    end
  end

  def stepthree
    must_be_signed_in
    @game = Game.find(params[:id])

    if @game.rounds.last.vote_count.to_s >= @game.rounds.last.submittedpictures.count.to_s && @game.rounds.last.final_picture.nil?
      count_round(@game, @game.rounds.last)
    end

    if @game.get_status == 1
      flickr_auth

      pictures = @game.rounds.last.submittedpictures

      @pictures = Array.new()

      pictures.each do|picture|
        Rails.logger.debug(picture)
        info = flickr.photos.getInfo(:photo_id => picture.flickr_id)
        @pictures.push(:photo => FlickRaw.url_b(info), :flickr_id => picture.flickr_id, :picture_id => picture.id)
      end
    end
  end

  private

  def count_round(game, round)
    round.submittedpictures.each do |picture|
      participant = game.participant(picture.user)

      if participant.score.nil?
        participant.score = 0
      end

      if picture.nrVotes.nil?
        picture.nrVotes = 0
      end

      multiplieValue = 5
      if(picture.user.id == round.storyteller.id)
        multiplieValue = 8
      end

      participant.score = participant.score + (picture.nrVotes * multiplieValue)
      participant.save
    end

    submittecpicture = round.max_votes_picture
    submittecpicture.isFinalPicture = true
    submittecpicture.save

    notify_players(@game)

    Round.create(:game => @game)

    redirect_to @game
  end

  def notify_players(game)
    game.participants.each do |participant|
      Message.create(:user_id => participant.user.id, :title => "Ronde ##{game.rounds.count} is voorbij", :text => "De ronde ##{game.rounds.count} is afgelopen, bekijk je scores!", :round_id => game.rounds.last.id, :read => false)
    end

    flash[:succes] = "Het spel is afgelopen. Kijk in het speloverzicht wat de score is geworden!"
  end
end
