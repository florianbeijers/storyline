# encoding: utf-8
class SubmittedpicturesController < AppController
  def select_picture
    round = Round.find(params[:id])
    game = round.game

    if round.submittedpictures.size >= game.participants.size
      flash[:error] = "Alle foto's zijn al toegevoegd!"
      redirect_to game
    else
      @submittedpicture = Submittedpicture.new(:user => User.find(params[:user_id]), :isStartPicture => params[:writer], :isFinalPicture => false, :flickr_id => params[:flickr_id], :round_id => params[:id])

      if @submittedpicture.save
        if round.submittedpictures.size >= game.participants.size
          notify_players(game)
        end
        flash[:success] = "Foto is succesvol geselecteerd!"
        redirect_to game
      else
        flash[:error] = "Er is een fout opgetreden, probeer opnieuw."
        redirect_to game_step_two_path
      end
    end
  end

  def show
    flickr_auth

    @pics = Array.new()

    info = flickr.photos.getInfo(:photo_id => params[:id])
    @pics.push(:photo => FlickRaw.url_b(info))

    @picture = @pics[0]
  end

  def select
    flickr_auth
    pictureArray = flickr.photos.search(:text => params[:string], :per_page => 10, :page => 1)

    if params[:writer].nil?
      writer = 0
    else
      writer = params[:writer]
    end

    @pics = Array.new()

    pictureArray.each do|foto|
      info = flickr.photos.getInfo(:photo_id => foto.id)
      @pics.push(:photo => FlickRaw.url_b(info), :params => params, :flickr_id => foto.id, :writer => writer)
    end

    render json: @pics
  end

  def vote_picture
    add_vote(params[:id])
    redirect_to current_user
  end

  def add_vote(id)
    @submittedpicture = Submittedpicture.find(id)

    if @submittedpicture.nrVotes.nil?
      @submittedpicture.nrVotes = 0
    end

    @submittedpicture.nrVotes = @submittedpicture.nrVotes + 1
    @submittedpicture.save
  end

  def notify_players(game)
    game.participants.each do |participant|
      Message.create(:user_id => participant.user.id, :title => "Fotos ingeleverd van ronde ##{game.rounds.count}.", :text => "Alle foto's van ronde ##{game.rounds.count} zijn ingeleverd! Open het spel om te gaan stemmen!ß", :round_id => game.rounds.last.id, :read => false)
    end

    flash[:succes] = "De foto's zijn ingeleverd, en de andere deelnemers hebben de melding ontvangen om hun stem uit te brengen"
  end
end
