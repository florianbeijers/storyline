class UsersController < AppController
  def new
    @user = User.new
  end

  def show
    #must_be_signed_in
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "U bent succesvol aangemeld, u kunt nu inloggen!"
      redirect_to sign_in_path
    else
      flash[:error] = "Er is een fout opgetreden, probeer opnieuw."
      flash[:error] = @user.errors.full_messages.first if @user.errors.any?
      render 'new'
    end
  end

  def messages
    @messages = current_user.messages
  end

  def read_message
    @message = Message.find(params[:id])

    if !@message.friend_id.nil? && @message.friend_id > 0
      @friend = User.find(@message.friend_id)
    end

    if !@message.game_id.nil? && @message.game_id > 0
      @game = Game.find(@message.game_id)
    end

    @message.read = true
    @message.save
  end

  def status_message
    @message = Message.find(params[:id])

    redirect = user_messages_path

    flash[:succes] = "Bericht is succesvol verwijderd!"

    if !@message.friend_id.nil? && @message.friend_id > 0
        if params[:status] == 1.to_s
        friend = User.find(@message.friend_id)
        current_user.add_friend(friend)
        flash[:success] = "Vriend is succesvol toegevoegd!"
      else
        flash[:error] = "Vriendschapverzoek is succesvol geweigerd!"
      end
    end

    if !@message.game_id.nil? && @message.game_id > 0
      if params[:status] == 1.to_s
        Participant.create!(:user_id => current_user.id, :game_id => @message.game_id)
        flash[:success] = "Storyline is succesvol toegevoegd!"
      else
        flash[:error] = "Verzoek tot het spelen van Storyline is succesvol geweigerd!"
      end
    end

    if !@message.round_id.nil? && @message.round_id > 0
      if params[:status] == 1.to_s
        round = Round.find(@message.round_id)
        redirect = round.game
      else
        flash[:error] = "Iets is er mis gegaan"
      end
    end

    @message.destroy
    redirect_to redirect
  end

  def addfriend
    Message.create(:user_id => params[:id], :title => 'Vriendschapsverzoek', :text => 'Wil je vrienden zijn?', :friend_id => current_user.id, :read => false)
    flash[:success] = "Vriendschapsverzoek is succesvol verzonden!"
    redirect_to :back
  end

  #TO model
  def search
    string = "%" + params[:string] + "%"

    userArray = Array.new

    #make sure there are no friends and know friends in this list.
    User.where("name like ? or email like ?", string, string).each do |user|
      if current_user.friends.exclude?(user) && current_user.known_players.exclude?(user)
        userArray.push(user)
      end
    end

    render json: userArray
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
