class AppController < ApplicationController
  attr_writer :current_user

  def index
    @games = Game.all
    @users = User.all
  end

  def sign_in(user)
    session[:current_user_id] = user.id
    self.current_user = user
  end

  def current_user
    @_current_user ||= session[:current_user_id] && User.find_by_id(session[:current_user_id])
  end

  def signed_in?
    !current_user.nil?
  end

  def sign_out
    @_current_user = session[:current_user_id] = nil
  end

  def must_be_signed_in
    unless signed_in?
      flash[:error] = "Jij hoort hier niet eens te kunnen komen. Hup, eerst aanmelden! ;)"
      redirect_to root_path
    end
  end

  def flickr_auth
    FlickRaw.api_key        = '3418ec572e6276e468fdce42da22c027'
    FlickRaw.shared_secret  = '3418ec572e6276e468fdce42da22c027'
  end
end
