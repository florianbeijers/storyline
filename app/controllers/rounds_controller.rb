class RoundsController < AppController
  def add_text
    @round = Round.find(params[:id])
    @round.storyFragment = round_params[:storyFragment]
    if @round.save
      flash[:success] = "Story text succesvol toegevoegd!"
      redirect_to game_step_two_writer_path(@round.game.id, 1)
    else
      flash[:error] = "Er is een fout opgetreden, probeer opnieuw."
      render 'new'
    end
  end

  def round_params
    params.require(:round).permit(:storyFragment)
  end
end
