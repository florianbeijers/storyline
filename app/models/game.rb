class Game < ActiveRecord::Base
  has_many :participants
  has_many :users, through: :participants
  has_many :rounds

  def participant(user)
    Participant.where(:game_id => self.id, :user_id => user.id)[0]
  end

  def get_status
    last_round = self.rounds.last

    unless last_round.nil?
      if last_round.submittedpictures.count >= self.participants.count
        return 1
      end

      if last_round.storyFragment != ''
        return 2
      end

      if last_round.storyFragment == ''
        return 3 # I freely admit that I don't really understand what I was doing when I wrote this now I look back at it ...but it doesn't break
      end
    end

    100
  end
end
