class User < ActiveRecord::Base
  has_many :participant
  has_many :games, through: :participant
  has_many :friendships
  has_many :submittedpictures
  has_many :messages

  has_many :friends, through: :friendships #, source: :friend

  before_save { self.email = email.downcase }
  has_secure_password

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates_format_of :email, :with => /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/ # look at me rocking awesome regular expression logic! ...no seriously I hate that stuff

  def known_players
    players = Array.new()
    self.games.each do |game|
      game.participants.each do |friend|
        if players.exclude?(friend.user)
          players.push(friend.user)
        end
      end
    end

    players
  end

  def unread_messages
    Message.where(:user_id => self.id, :read => false).order("created_at ASC")
  end

  def is_friend?(user)
    if user != self
      return !user.friends.include?(self) # that looks odd for some reason ...is that just me?
    end

    false
  end

  #Add a friend synchronized.
  def add_friend(user)
    #Check if user is really a user. Also, friend yet?
    if user.instance_of?(User)
      user.friends << self
      user.save

      self.friends << user
      self.save
    else
      flash[:error] = 'Vriend kon niet worden toegevoegd.'
    end
  end
end
