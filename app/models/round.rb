class Round < ActiveRecord::Base
  belongs_to :game
  has_many :submittedpictures

  def storyteller
    submittedpicture = Submittedpicture.where(:isStartPicture => "t", :round_id => self.id)[0]
    if submittedpicture.nil?
      return nil
    end

    User.find(submittedpicture[:user_id])
  end

  def final_picture
    Submittedpicture.where(:isFinalPicture => "t", :round_id => self.id)[0]
  end

  def max_votes_picture
    pictures = Submittedpicture.where(:round_id => self.id)
    max = 0
    returnPicture = Submittedpicture.new

    pictures.each do |picture|
      if picture.nrVotes.nil?
        picture.nrVotes = 0
      end

      if picture.nrVotes >= max
        max = picture.nrVotes
        returnPicture = picture # neatly calculating the highest amount of votes and such
      end
    end

    returnPicture
  end

  def vote_count
    self.submittedpictures.sum(:nrVotes)
  end
end
