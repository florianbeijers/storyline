class Message < ActiveRecord::Base
  belongs_to :user
  belongs_to :game
  belongs_to :friend
  belongs_to :round
end
