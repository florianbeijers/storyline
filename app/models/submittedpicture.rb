class Submittedpicture < ActiveRecord::Base
  belongs_to :round
  belongs_to :user

  def get_link
    require 'flickraw'
    FlickRaw.api_key        ='3418ec572e6276e468fdce42da22c027'
    FlickRaw.shared_secret  = '20e5ad3d6b27e6c4'

    @pictures = Array.new()

    info = flickr.photos.getInfo(:photo_id => self.flickr_id)
    @pictures.push(:photo => FlickRaw.url_b(info), :flickr_id => self.flickr_id, :picture_id => self.id)

    @pictures[0]
  end
end
